import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final title;
  final function;
  final mdw ;
  final heigth ;

  const MyButton({ required this.function,required this.title ,required this.mdw, this.heigth}) ;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        color: Colors.blue,
        height: heigth,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        onPressed: function,
        minWidth: mdw/2.0,
        child: Text("$title" , style: TextStyle(color: Colors.white , fontSize: 18),));
  }
}
