import "package:flutter/material.dart";
class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            child:  Container(
              decoration: BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage("assets/images/applogo.jpeg"),
                  fit: BoxFit.cover,
                ),
          ),),),
          SizedBox(
            height: 50,
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.account_circle,size: 35,color: Colors.indigo,),
              SizedBox(width: 8,),
                Text('Profile',style: TextStyle(fontSize: 28,color: Colors.indigo),),
              ],
            ),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.info,size: 35,color: Colors.indigo,),
                SizedBox(width: 8,),
                Text('About Us',style: TextStyle(fontSize: 28,color: Colors.indigo),),
              ],
            ),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.settings,size: 35,color: Colors.indigo,),
                SizedBox(width: 8,),
                Text('Settings',style: TextStyle(fontSize: 28,color: Colors.indigo),),
              ],
            ),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.logout ,size: 35,color: Colors.indigo,),
                SizedBox(width: 8,),
                Text('Sign Out',style: TextStyle(fontSize: 28,color: Colors.indigo),),
              ],
            ),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );

  }
}


